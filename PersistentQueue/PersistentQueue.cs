using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace PersistentQueue
{
    /// <summary>
    /// Represents a strongly typed persistent queue.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    [Serializable]
    [DebuggerDisplay("Count = {" + nameof(Count) + "}")]
    [DebuggerTypeProxy(typeof(GenericCollectionDebugView<>))]
    public class PersistentQueue<T> : ICollection<PersistentQueueVersion<T>>, IReadOnlyCollection<PersistentQueueVersion<T>>, ICollection
    {
        /// <summary>
        /// Retrieves the current collection version by the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        public PersistentQueueVersion<T> this[int index] => versions[index];

        /// <summary>
        /// The elements count.
        /// </summary>
        public int Count => versions.Count;

        /// <summary>
        /// Indicates whether the current collection is a synchronized one.
        /// </summary>
        public bool IsSynchronized => false;
        
        /// <summary>
        /// The synchronization object.
        /// </summary>
        [field: NonSerialized]
        public object SyncRoot { get; } = new object();
        
        /// <summary>
        /// Indicates whether the current collection is a readonly one.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Constructs the new instance of PersistentQueue from the specified collection.
        /// </summary>
        /// <param name="collection">The new item.</param>
        /// <exception cref="ArgumentNullException">This exception is thrown when other is null.</exception>
        public PersistentQueue(IEnumerable<T> collection)
        {
            if (collection is null)
                throw new ArgumentNullException($"{nameof(collection)} doesn't refers to the particular object because it is null");
            
            versions.Add(EmptyVersion);
            foreach (T item in collection)
                Enqueue(item);
        }
        
        /// <summary>
        /// Constructs the new empty instance of PersistentQueue.
        /// </summary>
        public PersistentQueue()
        {
            versions.Add(EmptyVersion);
        }
        
        /// <summary>
        /// Adds new version with the specified new item.
        /// </summary>
        /// <param name="item">The new item.</param>
        public void Enqueue(T item)
        {
            int last = versions.Count - 1;
            Node<T> newHead = versions[last].Head;
            Node<T> newTail = versions[last].Tail;
            int newCount = versions[last].Count;
            
            Node<T> newNode = new Node<T>(item);
            if (newCount == 0)
                newHead = newTail = newNode;
            else
            {
                newTail.Next = newNode;
                newTail = newNode;
            }
            newCount++;
            
            versions.Add(new PersistentQueueVersion<T>(newHead, newTail, newCount)
            {
                VersionNumber = versions.Count - 1
            });
        }

        /// <summary>
        /// Retrieves the first added item into the current collection and removes it from the current collection.
        /// </summary>
        /// <exception cref="InvalidOperationException">This exception is thrown when the last version of the current collection is empty.</exception>
        /// <returns>The first added item.</returns>
        public T Dequeue()
        {
            int last = versions.Count - 1;
            Node<T> newHead = versions[last].Head;
            Node<T> newTail = versions[last].Head;
            int newCount = versions[last].Count;

            if (newCount == 0)
                throw new InvalidOperationException($"{nameof(Count)} was zero");

            T toReturn = newHead.Value;
            if (newCount == 1)
                newTail = null;
            newHead = newHead.Next;
            newCount--;
            
            versions.Add(new PersistentQueueVersion<T>(newHead, newTail, newCount)
            {
                VersionNumber = versions.Count - 1
            });
            return toReturn;
        }

        void ICollection<PersistentQueueVersion<T>>.Add(PersistentQueueVersion<T> item)
        {
            throw new NotSupportedException();
        }

        bool ICollection<PersistentQueueVersion<T>>.Remove(PersistentQueueVersion<T> item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Clears the current collection.
        /// </summary>
        public void Clear()
        {
            versions.Clear();
            versions.Add(EmptyVersion);
        }

        /// <summary>
        /// Searches the particular item in the current collection.
        /// </summary>
        /// <param name="item">The searched for item.</param>
        /// <returns>true, if the searched for item presents in the current collection, false otherwise.</returns>
        public bool Contains(PersistentQueueVersion<T> item)
        {
            return versions.Contains(item);
        }
        
        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        public void CopyTo(PersistentQueueVersion<T>[] array, int arrayIndex)
        {
            versions.CopyTo(array, arrayIndex);
        }
        
        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            ((ICollection)versions).CopyTo(array, arrayIndex);
        }
        
        /// <summary>
        /// Retrieves the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator of the current collection.</returns>
        public IEnumerator<PersistentQueueVersion<T>> GetEnumerator()
        {
            return versions.GetEnumerator();
        }

        /// <summary>
        /// Retrieves the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator of the current collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        internal readonly List<PersistentQueueVersion<T>> versions = new List<PersistentQueueVersion<T>>();
        private readonly PersistentQueueVersion<T> EmptyVersion = new PersistentQueueVersion<T>(null, null, 0);
    }
}