using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PersistentQueue
{
    /// <summary>
    /// Represents a proxy-type for a generic collection.
    /// </summary>
    /// <typeparam name="T">The value type.</typeparam>
    internal class GenericCollectionDebugView<T>
    {
        /// <summary>
        /// Constructs the new instance of GenericCollectionDebugView from the specified collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <exception cref="ArgumentNullException">This exception is thrown when collection is null.</exception>
        internal GenericCollectionDebugView(ICollection<T> collection)
        {
            if (collection is null)
                throw new ArgumentNullException($"{nameof(collection)} doesn't refers to the particular object because it is null");
            source = collection;
        }
        
        /// <summary>
        /// The collection items.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                T[] array = new T[source.Count];
                source.CopyTo(array, 0);
                return array;
            }
        }
        
        private ICollection<T> source;
    }
}