namespace PersistentQueue
{
    /// <summary>
    /// Represents a version of a versions collection.
    /// </summary>
    public interface IVersionInfo
    {
        /// <summary>
        /// The version number.
        /// </summary>
        int Version { get; }
        
        /// <summary>
        /// Indicates whether the current version is modifiable.
        /// </summary>
        bool IsReadOnly { get; }
    }
}