using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace PersistentQueue
{
    /// <summary>
    /// Represents a persistent queue version.
    /// </summary>
    [Serializable]
    [DebuggerDisplay("Count = {" + nameof(Count) + "}")]
    [DebuggerTypeProxy(typeof(GenericCollectionDebugView<>))]
    public sealed class PersistentQueueVersion<T> : ICollection<T>, IReadOnlyCollection<T>, ICollection, IFormattable
    {
        /// <summary>
        /// The version info.
        /// </summary>
        public int VersionNumber { get; internal set; }
        
        /// <summary>
        /// The elements count.
        /// </summary>
        public int Count { get; }

        /// <summary>
        /// Indicates whether the current collection is a synchronized one.
        /// </summary>
        public bool IsSynchronized => false;

        /// <summary>
        /// The synchronization object.
        /// </summary>
        [field: NonSerialized]
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Indicates whether the current collection is a readonly one.
        /// </summary>
        public bool IsReadOnly => VersionNumber == source.versions.Count - 1;

        /// <summary>
        /// The head.
        /// </summary>
        public Node<T> Head { get; }
        
        /// <summary>
        /// The tail.
        /// </summary>
        public Node<T> Tail { get; }
        
        /// <summary>
        /// Constructs the new instance of PersistentQueueVersion from the particular head, tail and count.
        /// </summary>
        /// <param name="head">The head.</param>
        /// <param name="tail">The tail.</param>
        /// <param name="count">The count.</param>
        internal PersistentQueueVersion(Node<T> head, Node<T> tail, int count)
        {
            Head = head;
            Tail = tail;
            Count = count;
        }

        /// <summary>
        /// Returns the first added into the current collection value.
        /// </summary>
        /// <returns>The first added item.</returns>
        /// <exception cref="InvalidOperationException">This exception is thrown when the last version of the current collection is empty.</exception>
        public T Peek()
        {
            if (Count == 0)
                throw new InvalidOperationException($"{nameof(Count)} was zero");
            return Head.Value;
        }

        void ICollection<T>.Add(T item)
        {
            throw new NotSupportedException();
        }

        bool ICollection<T>.Remove(T item)
        {
            throw new NotSupportedException();
        }

        void ICollection<T>.Clear()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Searches the particular item in the current collection.
        /// </summary>
        /// <param name="item">The searched for item.</param>
        /// <returns>true, if the searched for item presents in the current collection, false otherwise.</returns>
        public bool Contains(T item)
        {
            Node<T> current = Head;
            while (!(current is null) && !current.Value.Equals(item))
                current = current.Next;
            return !(current is null);
        }

        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            EvaluateArrayCorrectness(array, arrayIndex);
            Node<T> current = Head;
            while (!(current is null))
            {
                array[arrayIndex++] = current.Value;
                current = current.Next;
            }
        }

        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        /// <exception cref="ArgumentException">This exception is thrown when the array has non-zero lower bound.</exception>
        /// <exception cref="RankException">This exception is thrown when the array is not a 1-dimension one.</exception>
        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            EvaluateArrayCorrectness(array, arrayIndex);
            if (array.Rank != 1)
                throw new RankException("{nameof(array)} is not a 1-dimension array");
            if (array.GetLowerBound(0) != 0)
                throw new ArgumentException("{nameof(array)} has non-zero lower bound");
            
            Node<T> current = Head;
            while (!(current is null))
            {
                array.SetValue(current.Value, arrayIndex++);
                current = current.Next;
            }
        }

        /// <summary>
        /// Retrieves the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator of the current collection.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }
        
        /// <summary>
        /// Retrieves the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator of the current collection.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Retrieves the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator of the current collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns the string representation of the current instance of PersistentQueueVersion.
        /// </summary>
        /// <returns>The string representation of the current instance of PersistentQueueVersion.</returns>
        public override string ToString()
        {
            return ToString(null, null);
        }

        /// <summary>
        /// Returns the string representation of the current instance of PersistentQueueVersion.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="formatProvider">The format settings provider.</param>
        /// <returns>The string representation of the current instance of PersistentQueueVersion.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            string valueToString(Node<T> node)
            {
                return node == null ? "null" : node.ToString();
            }
            
            format = format ?? DefaultFormatString;
            formatProvider = formatProvider ?? defaultFormatProvider;
            return string.Format(formatProvider, format, valueToString(Head), valueToString(Tail));
        }
        
        private void EvaluateArrayCorrectness(Array array, int index)
        {
            if (array is null)
                throw new ArgumentNullException($"{nameof(array)} doesn't refers to the particular object because it is null");
            if ((index < 0) || (index >= array.Length))
                throw new ArgumentOutOfRangeException($"{nameof(index)} must be nonnegative number which is less than the collection elements count");
            if (array.Length - index < Count)
                throw new ArgumentOutOfRangeException($"{nameof(array)} can't be filled from the current collection because there is no much place to store the entire collection");
        }
        
        private const string DefaultFormatString = "Head = {0}, Tail = {1}";

        private readonly PersistentQueue<T> source;
        private readonly IFormatProvider defaultFormatProvider = CultureInfo.CurrentCulture;
        
        /// <summary>
        /// The enumerator.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            /// <summary>
            /// The current item.
            /// </summary>
            public T Current
            {
                get
                {
                    if (!enumerationStarted)
                        throw new InvalidOperationException(nameof(enumerationStarted));
                    return current.Value;
                }
            }

            /// <summary>
            /// The current item.
            /// </summary>
            object IEnumerator.Current => Current;
            
            /// <summary>
            /// Constructs the new instance of Enumerator from the particular persistent queue version.
            /// </summary>
            /// <param name="persistentQueueVersion">The persistent queue version.</param>
            /// <exception cref="ArgumentNullException">This exception is thrown when persistentQueueVersion is null.</exception>
            public Enumerator(PersistentQueueVersion<T> persistentQueueVersion) : this()
            {
                if (persistentQueueVersion is null)
                    throw new ArgumentNullException(nameof(persistentQueueVersion));
                source = persistentQueueVersion;
            }

            /// <summary>
            /// Moves the current enumerator on one unit and returns the value that indicated whether futher motion is possible.
            /// </summary>
            /// <returns></returns>
            /// <exception cref="NotImplementedException"></exception>
            public bool MoveNext()
            {
                if (!enumerationStarted)
                {
                    enumerationStarted = true;
                    current = source.Head;
                }
                else
                    current = current.Next;

                index++;
                return index < source.Count;
            }

            /// <summary>
            /// Resets the enumerator.
            /// </summary>
            /// <exception cref="NotImplementedException"></exception>
            public void Reset()
            {
                enumerationStarted = false;
                index = 0;
                current = null;
            }
            
            /// <summary>
            /// Releases all used by the current enumerator unmanaged resources.
            /// </summary>
            public void Dispose()
            {
            }
            
            private readonly PersistentQueueVersion<T> source;
            private bool enumerationStarted;
            private int index;
            private Node<T> current;
        }
    }
}